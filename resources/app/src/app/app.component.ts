import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ipcRenderer, remote} from 'electron';

let {dialog} = remote;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild('pathInput') pathInput: ElementRef;

  modules = [{
    name: 'Evidence Planning',
    path: 'pia-core-evidence-planning\\pia-core-evidence-planning-client',
    srcPath: 'pia-core-evidence-planning\\pia-core-evidence-planning-client\\src\\certification',
    selected: true
  }, {
    name: 'Evidence Validation',
    path: 'pia-core-evidence-validation\\pia-core-evidence-validation-client',
    srcPath: 'pia-core-evidence-validation\\pia-core-evidence-validation-client\\src\\pf',
    selected: true
  }, {
    name: 'Ppar',
    path: 'pia-core-ppar\\pia-core-ppar-client',
    srcPath: 'pia-core-ppar\\pia-core-ppar-client\\src\\ppar',
    selected: true
  }, {
    name: 'QMI',
    path: 'pia-core-qmi\\pia-core-qmi-client',
    srcPath: 'pia-core-qmi\\pia-core-qmi-client\\src\\qmi',
    selected: true
  }, {
    name: 'QMI-Part',
    path: 'pia-core-qmi-part\\pia-core-qmi-part-client',
    srcPath: 'pia-core-qmi-part\\pia-core-qmi-part-client\\src\\qmi-part',
    selected: true
  }, {
    name: 'Supplier',
    path: 'pia-core-supplier\\pia-core-supplier-client',
    srcPath: 'pia-core-supplier\\pia-core-supplier-client\\src\\supplier',
    selected: true
  }];

  labels = [
    {name: 'DDL Permissions', id: 'DDL_PERMISSION', selected: false},
    {name: 'DML Permissions', id: 'DML_PERMISSION', selected: false},
    {name: 'Supplier Data', id: 'SUPPLIER_DATA', selected: false},
    {name: 'Part Data', id: 'PART_DATA', selected: false},
    {name: 'Approval tree Master Data', id: 'APPROVAL_TREE_MASTER_DATA', selected: false},
    {name: 'Certification Data', id: 'CERTIFICATION_DATA', selected: false},
    {name: 'Business Data', id: 'BUSINESS_DATA', selected: true}];

  path = '';

  ngOnInit(): void {
  }

  start(): void {
    const selectedModules = this.modules.filter(m => m.selected === true);
    if (selectedModules.length > 0) {
      ipcRenderer.send('run-dev', selectedModules);
    }
  }

  update(): void {
    const selectedModules = this.modules.filter(m => m.selected === true);
    if (selectedModules.length > 0) {
      ipcRenderer.send('update', selectedModules);
    }
  }

  setLabels(): void {
    const selectedLabels = this.labels.filter(l => l.selected === true).map(l => l.id);
    ipcRenderer.send('script-labels', selectedLabels);

  }

  openDialog(): void {
    dialog.showOpenDialog({properties: ['openDirectory']}, (data) => {
      if (data && data[0]) {
        this.path = data[0];
        this.pathInput.nativeElement.focus();
        ipcRenderer.send('project-path', {path: data[0]});
      }
    });
  }

  resetLicense(): void {
    ipcRenderer.send('reset-license');
  }

  createLink(): void {
    const selectedModules = this.modules.filter(m => m.selected === true);
    if (selectedModules.length > 0) {
      ipcRenderer.send('create-link', selectedModules);
    }
  }
}
