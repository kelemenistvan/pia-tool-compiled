const {app, BrowserWindow, ipcMain} = require('electron');
const fs = require('fs');
let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 850,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`
  });
  win.loadURL(`file://${__dirname}/dist/index.html`);
  //// uncomment below to open the DevTools.
  // win.webContents.openDevTools()
  // Event when the window is closed.
  win.on('closed', function () {
    win = null
  });

  ipcMain.on('update', (event, arg) => {
    let cmd = `start cmd /K "cd /d ${path}`;
    arg.forEach(module => {
      cmd = cmd + ` && npm update --prefix ${module.path}`;
    });
    cmd = cmd + '"';
    execute(cmd);

  });

  ipcMain.on('run-dev', (event, arg) => {
    arg.forEach(module => {
      let cmd = `start cmd /K "cd /d ${path}\\${module.path} && npm run dev"`;
      execute(cmd);
    });
  });

  ipcMain.on('project-path', (event, arg) => {
    console.log(`msg: ${arg.path}`);
    path = arg.path;
  });

  ipcMain.on('script-labels', (event, arg) => {

    let labels = arg.length > 0 ? arg.join(', ') : 'NONE';
    const propertyPath = `${path}\\pia-core-db\\pia-core-db-migration\\src\\main\\resources\\application.properties`;
    console.log(`Setting labels: ${labels}`);
    replaceInFile(propertyPath, /liquibase.labels=.*/, `liquibase.labels=${labels}`);

  });

  ipcMain.on('reset-license', () => {
    execute('reg delete "HKEY_CURRENT_USER\\Software\\JavaSoft\\Prefs\\jetbrains\\idea" /f');
    for (let i = 1; i <= 3; i++) {
      execute(`del ${process.env['USERPROFILE']}\\.IntelliJIdea2017.${i}\\config\\eval\\*.key`);
      replaceInFile(`${process.env['USERPROFILE']}\\.IntelliJIdea2017.${i}\\config\\options\\options.xml`, /evlsprt.*/, '');
    }
  });

  ipcMain.on('create-link', (event, arg) => {
    const commonPath = `${path}\\pia-core-common\\pia-core-common-client\\src`;

    arg.forEach(module => {
      let cmd = `start cmd /C "cd /d ${path}`;
      cmd = cmd + ` && if exist ${module.srcPath}\\pia rmdir ${module.srcPath}\\pia`;
      cmd = cmd + ` && if exist ${module.srcPath}\\s0ssqms rmdir ${module.srcPath}\\s0ssqms`;
      cmd = cmd + ` && mklink /D ${module.srcPath}\\pia ${commonPath}\\pia`;
      cmd = cmd + ` && mklink /D ${module.srcPath}\\s0ssqms ${commonPath}\\s0ssqms`;
      cmd = cmd + '"';
      execute(cmd);
    });
  });
}

// Create window on electron intialization
app.on('ready', () => {
  createWindow();
  execute('npm config set registry https://ps41.ps.msg.de/nexus/content/groups/npm-all');
  // execute('npm config set strict-ssl false');
});
// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
});
app.on('activate', () => {
  // macOS specific close process
  if (win === null) {
    createWindow()
  }
});

const exec = require('child_process').exec;
let path = 'C:\\';

function execute(cmd) {
  console.log(`executing: ${cmd}`);
  exec(cmd, (error, stdout, stderr) => {
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  });
}

function replaceInFile(filePath, regex, string) {
  fs.readFile(filePath, 'utf-8', (err, data) => {
      if (err)
        console.log(err);
      else {
        console.log(`Replacing ${regex} with ${string}`);
        const newData = data.replace(regex, string);
        fs.writeFile(filePath, newData, 'utf-8', function (err) {
          console.log(err ? err : 'write complete');
        });
      }

    }
  );
}
