webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".center-grid {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  display: grid;\n}\n\n.center-flex {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.path-input {\n  width: 450px;\n}\n\n.pia-logo {\n  width: 40px;\n  margin-top: 20px;\n  margin-right: 10px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"center-flex\">\n <span>\n    <img class=\"pia-logo\" alt=\"PIA Logo\"\n         src=\"assets/pia.png\">\n </span>\n  <h1>\n    PIA Dev Tool\n  </h1>\n</div>\n\n<br>\n<br>\n<br>\n\n<div class=\"center-flex\">\n  <button mat-raised-button (click)=\"openDialog()\"> Select project home</button>\n</div>\n<div class=\"center-flex\">\n  <mat-form-field class=\"path-input\">\n    <input #pathInput matInput [(ngModel)]=\"path\" width=\"600\">\n  </mat-form-field>\n</div>\n\n<div>\n  <mat-grid-list cols=\"2\">\n    <mat-grid-tile>\n      <mat-list>\n        <mat-list-item *ngFor=\"let module of modules\">\n          <mat-checkbox [(ngModel)]=\"module.selected\">\n            {{module.name}}\n          </mat-checkbox>\n        </mat-list-item>\n        <mat-list-item>\n          <div>\n            <button mat-raised-button (click)=\"update()\" [disabled]=\"path==''\"> Update modules</button>\n            <button mat-raised-button (click)=\"start()\" [disabled]=\"path==''\"> Start apps</button>\n          </div>\n        </mat-list-item>\n        <mat-list-item>\n          <div>\n            <button mat-raised-button (click)=\"createLink()\" [disabled]=\"path==''\"> Link to core-common\n            </button>\n          </div>\n        </mat-list-item>\n      </mat-list>\n    </mat-grid-tile>\n\n    <mat-grid-tile>\n      <mat-list>\n        <mat-list-item *ngFor=\"let label of labels\">\n          <mat-checkbox [(ngModel)]=\"label.selected\">\n            {{label.name}}\n          </mat-checkbox>\n        </mat-list-item>\n        <mat-list-item>\n          <div>\n            <button mat-raised-button (click)=\"setLabels()\" [disabled]=\"path==''\"> Set script labels</button>\n          </div>\n        </mat-list-item>\n      </mat-list>\n    </mat-grid-tile>\n  </mat-grid-list>\n\n</div>\n<br><br>\n<div class=\"center-flex\">\n  <button mat-raised-button (click)=\"resetLicense()\"> Reset IntelliJ IDEA license</button>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_electron__ = __webpack_require__("electron");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_electron___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_electron__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var dialog = __WEBPACK_IMPORTED_MODULE_1_electron__["remote"].dialog;
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.modules = [{
                name: 'Evidence Planning',
                path: 'pia-core-evidence-planning\\pia-core-evidence-planning-client',
                srcPath: 'pia-core-evidence-planning\\pia-core-evidence-planning-client\\src\\certification',
                selected: true
            }, {
                name: 'Evidence Validation',
                path: 'pia-core-evidence-validation\\pia-core-evidence-validation-client',
                srcPath: 'pia-core-evidence-validation\\pia-core-evidence-validation-client\\src\\pf',
                selected: true
            }, {
                name: 'Ppar',
                path: 'pia-core-ppar\\pia-core-ppar-client',
                srcPath: 'pia-core-ppar\\pia-core-ppar-client\\src\\ppar',
                selected: true
            }, {
                name: 'QMI',
                path: 'pia-core-qmi\\pia-core-qmi-client',
                srcPath: 'pia-core-qmi\\pia-core-qmi-client\\src\\qmi',
                selected: true
            }, {
                name: 'QMI-Part',
                path: 'pia-core-qmi-part\\pia-core-qmi-part-client',
                srcPath: 'pia-core-qmi-part\\pia-core-qmi-part-client\\src\\qmi-part',
                selected: true
            }, {
                name: 'Supplier',
                path: 'pia-core-supplier\\pia-core-supplier-client',
                srcPath: 'pia-core-supplier\\pia-core-supplier-client\\src\\supplier',
                selected: true
            }];
        this.labels = [
            { name: 'DDL Permissions', id: 'DDL_PERMISSION', selected: false },
            { name: 'DML Permissions', id: 'DML_PERMISSION', selected: false },
            { name: 'Supplier Data', id: 'SUPPLIER_DATA', selected: false },
            { name: 'Part Data', id: 'PART_DATA', selected: false },
            { name: 'Approval tree Master Data', id: 'APPROVAL_TREE_MASTER_DATA', selected: false },
            { name: 'Certification Data', id: 'CERTIFICATION_DATA', selected: false },
            { name: 'Business Data', id: 'BUSINESS_DATA', selected: true }
        ];
        this.path = '';
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.start = function () {
        var selectedModules = this.modules.filter(function (m) { return m.selected === true; });
        if (selectedModules.length > 0) {
            __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('run-dev', selectedModules);
        }
    };
    AppComponent.prototype.update = function () {
        var selectedModules = this.modules.filter(function (m) { return m.selected === true; });
        if (selectedModules.length > 0) {
            __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('update', selectedModules);
        }
    };
    AppComponent.prototype.setLabels = function () {
        var selectedLabels = this.labels.filter(function (l) { return l.selected === true; }).map(function (l) { return l.id; });
        __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('script-labels', selectedLabels);
    };
    AppComponent.prototype.openDialog = function () {
        var _this = this;
        dialog.showOpenDialog({ properties: ['openDirectory'] }, function (data) {
            if (data && data[0]) {
                _this.path = data[0];
                _this.pathInput.nativeElement.focus();
                __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('project-path', { path: data[0] });
            }
        });
    };
    AppComponent.prototype.resetLicense = function () {
        __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('reset-license');
    };
    AppComponent.prototype.createLink = function () {
        var selectedModules = this.modules.filter(function (m) { return m.selected === true; });
        if (selectedModules.length > 0) {
            __WEBPACK_IMPORTED_MODULE_1_electron__["ipcRenderer"].send('create-link', selectedModules);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('pathInput'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AppComponent.prototype, "pathInput", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["H" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["b" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["d" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MatGridListModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ "electron":
/***/ (function(module, exports) {

module.exports = require("electron");

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map